<?php

declare(strict_types=1);

namespace Netzwolke\Logger;

use Illuminate\Database\Capsule\Manager;
use Psr\Log\LogLevel;

class Logger extends LogLevel implements NetzwolkeLoggerInterface
{

    /**
     * @inheritDoc
     */
    public function __construct(
        private DBManager $manager
    ) {
    }


    /**
     * @inheritDoc
     */
    public function emergency($message, array $context = array())
    {
        $this->log(self::EMERGENCY, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function alert($message, array $context = array())
    {
        $this->log(self::ALERT, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function critical($message, array $context = array())
    {
        $this->log(self::CRITICAL, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function error($message, array $context = array())
    {
        $this->log(self::ERROR, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function warning($message, array $context = array())
    {
        $this->log(self::WARNING, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function notice($message, array $context = array())
    {
        $this->log(self::NOTICE, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function info($message, array $context = array())
    {
        $this->log(self::INFO, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function debug($message, array $context = array())
    {
        $this->log(self::DEBUG, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = array())
    {
        $this->manager->addLog($level, $message, json_encode($context));
    }

    /**
     * @param string $level
     * @param string $type
     * @param int $code
     * @param string $message
     * @param string $file
     * @param string $line
     * @param string $trace
     */
    public function fire(
        string $level = 'error',
        string $type = 'Error',
        int $code = 0,
        string $message = 'No Message',
        string $file = 'No File',
        int $line = 0,
        string $trace = 'No Trace',
    ): void
    {
        $content = [
            'type' => $type,
            'code' => $code,
            'message' => $message,
            'file' => $file,
            'line' => $line,
            'trace' => $trace
        ];
        $this->$level(json_encode($content));
    }
}
