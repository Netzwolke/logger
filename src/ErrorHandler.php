<?php

declare(strict_types=1);

namespace Netzwolke\Logger;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Interfaces\ErrorHandlerInterface;
use Slim\Psr7\Response;
use Throwable;

class ErrorHandler implements ErrorHandlerInterface
{

    private ServerRequestInterface $request;
    private Throwable $exception;

    public function __construct(
        private LoggerInterface $logger
    ) {
    }
    /**
     * @inheritDoc
     */
    public function __invoke(
        ServerRequestInterface $request,
        Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails
    ): ResponseInterface {
        $this->request = $request;
        $this->exception = $exception;
        if ($logErrors) {
            $this->writeToErrorLog($exception);
        }

        return $this->respond($exception);
    }

    private function makeJsonMessage(Throwable $exception): string
    {
        return json_encode([
            'type' => get_class($exception),
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTrace()
        ]);
    }

    private function makeStringMessage(Throwable $exception): string
    {
        $text = sprintf("Type: %s\n", get_class($exception));

        $code = $exception->getCode();
        if ($code !== null) {
            $text .= sprintf("Code: %s\n", $code);
        }

        $message = $exception->getMessage();
        if ($message !== null) {
            $text .= sprintf("Message: %s\n", htmlentities($message));
        }

        $file = $exception->getFile();
        if ($file !== null) {
            $text .= sprintf("File: %s\n", $file);
        }

        $line = $exception->getLine();
        if ($line !== null) {
            $text .= sprintf("Line: %s\n", $line);
        }

        $trace = $exception->getTraceAsString();
        if ($trace !== null) {
            $text .= sprintf('Trace: %s', $trace);
        }
        return $text;
    }

    /**
     * @param Throwable $exception
     */
    private function writeToErrorLog(Throwable $exception): void
    {
        $this->logger->error($this->makeJsonMessage($exception));
    }

    private function respond(Throwable $exception): ResponseInterface
    {
        $response = new Response();
        $content = $this->makeJsonMessage($exception);
        $response->getBody()->write($content);
        return $response->withHeader('Content-Type', 'application/json');
    }
}
