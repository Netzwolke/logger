<?php

namespace Netzwolke\Logger;

use Psr\Log\LoggerInterface;

interface NetzwolkeLoggerInterface extends LoggerInterface
{
    public function fire(
        string $level = 'error',
        string $type = 'Error',
        int $code = 401,
        string $message = 'No Message',
        string $file = 'No File',
        int $line = 0,
        string $trace = 'No Trace',
    ): void;
}
