<?php

declare(strict_types=1);

namespace Netzwolke\Logger;

use Illuminate\Database\Capsule\Manager;

class DBManager
{
    public function __construct(
        private Manager $manager
    ) {
        $config = $this->manager->getConnection('base')->getConfig();
        $config['database'] = 'error_schema';
        $this->manager->addConnection($config, 'error');
        if ($this->DBisEmpty()) {
            $this->createDB();
        }
    }
    public function DBisEmpty(): bool
    {

        $array = Manager::schema('base')
            ->getConnection()
            ->select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'error_schema';");
        if (count($array) > 0) {
            return false;
        }
        return true;
    }
    public function createDB()
    {

        $this->manager->getConnection('base')
            ->statement("CREATE DATABASE IF NOT EXISTS error_schema;");
        $this->createTable();
    }

    public function createTable()
    {
        $this->manager->getConnection('error')->getSchemaBuilder()->create('errorLogs', function ($table) {
            $table->increments('id');
            $table->text('message');
            $table->string('details');
            $table->string('level');
            $table->dateTime('time');
        });
    }

    /**
     * @param $level
     * @param $message
     * @param $details
     */
    public function addLog($level, $message, $details)
    {
        $time = date('Y-m-d H:i:s');
        $this->manager
            ->getConnection('error')
            ->insert(
                "INSERT INTO errorLogs (`message`, `details`, `level`, `time`) values (:message, :details, :level, :time)",
                ['message' => $message, 'details' => $details, 'level' => $level, 'time' => $time]
            );
    }
}
